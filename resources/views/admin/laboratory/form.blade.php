@extends('admin.layout.app')
@section('content')
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/themes/smoothness/jquery-ui.css" />

        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->

                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Add new Laboratory</h3>

                    </div>
                    <div class="col-md-7 col-4 align-self-center">

                   </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
          
                	@if($lab->id>0)
			         <form  class="form-horizontal form-material" action="{{ url('/laboratory/'.$lab->id) }}" method="post" enctype='multipart/form-data'>
			           <input name="_method" type="hidden" value="patch">
			         @else
			         <form class="form-horizontal form-material" action="{{ url('/laboratory') }}" method="post" enctype='multipart/form-data'>
			       @endif
                  @if(count($errors))
                    @foreach ($errors->all() as $error)
                       <div class="alert alert-danger"><h4>{{ $error }}</h4></div>
                   @endforeach
                 @endif
                   
                        <div class="col">
                            <div class="card">
                                <div class="card-block">
                       
                                    <div class="row">
                                        <div class="col">
                                            <label class="col-md-12">Name </label>
                                            <div class="col-md-12">
                                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
               
                                               <input type="text" name="name"  value="{{ old('name', $lab->name) }}" class="form-control form-control-line"  required>
                                           </div>
                                        </div>
                                        <div class="col ">
                                           <label class="col-md-12">Code </label>
                                            <div class="col-md-12">
                                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                             
                                               <input type="text" name="code"  value="{{ old('code', $lab->code) }}" class="form-control form-control-line"  required >
                                           </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <label class="col-md-12">City </label>
                                            <div class="col-md-12">
                                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                               <input type="text"  name="city" value="{{ old('city', $lab->city) }}" style="text-transform:capitalize;" class="form-control form-control-line"  required >
                                           </div>
                                        </div>
                                        <div class="col ">
                                          <label class="col-md-12">Location</label>
                                            <div class="col-md-12">
                                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                               <input type="text"  name="location" value="{{ old('location', $lab->location) }}" style="text-transform:capitalize;" class="form-control form-control-line"  required >
                                           </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col">
                                            <label class="col-md-12">Details </label>
                                            <div class="col-md-12">
                                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                               <input type="text"  name="details" value="{{ old('details', $lab->details) }}" style="text-transform:capitalize;" class="form-control form-control-line">
                                           </div>
                                        </div>
                                        <div class="col ">
                                          
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col">
                                            <label class="col-md-12">In-Charge : Name  </label>
                                            <div class="col-md-12">
                                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                               <input type="text" name="in_charge" value="{{ old('in_charge', $lab->in_charge) }}" style="text-transform:capitalize;" class="form-control form-control-line"  required >
                                           </div>
                                        </div>
                                        <div class="col ">
                                          <label class="col-md-12">In-Charge : Email  </label>
                                            <div class="col-md-12">
                                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                               <input type="email" name="in_charge_email" value="{{ old('in_charge_email', $lab->in_charge_email) }}"  class="form-control form-control-line"  required >
                                           </div>
                                        </div>
                                    </div>

                                  

                                     <div class="row">
                                        <div class="col">
                                            <label class="col-md-12">In-Charge : Number  </label>
                                            <div class="col-md-12">
                                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                               <input type="text"  name="in_charge_number" value="{{ old('in_charge_number', $lab->in_charge_number) }}"  class="form-control form-control-line"  required >
                                           </div>
                                        </div>
                                        <div class="col ">
                                          
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-12">
                            <button class="btn btn-info">Save</button>

                        </div>

                    </div>           
                    </div>
                      
                </form>

      
            </div>
           
            <footer class="footer"> © Copyright Reserved 2017-2018, LIMS </footer>
         
        </div>



@endsection
