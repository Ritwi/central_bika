<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('/assets/images/favicon.png')}}">
    <title>LIMS</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{url('/assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

   <!--  <link href="{{url('/assets/plugins/chartist-js/dist/chartist.min.css') }}" rel="stylesheet">
    <link href="{{url('/assets/plugins/chartist-js/dist/chartist-init.css') }}" rel="stylesheet">
    <link href="{{url('/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css') }}" rel="stylesheet"> -->
    <!-- <link href="{{url('/assets/plugins/c3-master/c3.min.css') }}" rel="stylesheet"> -->
    <link href="{{url('css/style.css') }}" rel="stylesheet">
    <link href="{{url('css/side-menu.css') }}" rel="stylesheet">
    <link href="{{url('css/colors/blue.css') }}" id="theme" rel="stylesheet">
   <!--  <link id="bsdp-css" href="{{url('bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet"> -->
   <!--  <link href="{{ url('https://fonts.googleapis.com/css?family=Roboto:400,500') }}" rel="stylesheet"> -->
    <link href="{{ url('https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">

    <script src="{{ url('assets/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ url('https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js') }}
    "></script>
    <script src="{{ url('/assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
</head>


<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-toggleable-sm navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{ url('/dashboard') }}">
                        <!-- Logo icon --><b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->

                            <!-- Light Logo icon -->
                            <img src="{{ url('assets/images/logo-light-icon.png')}}" alt="homepage" class="light-logo" /></b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>

                         <!-- Light Logo text -->
                         <img src="{{ url('assets/images/logo-light-text.png')}}" class="light-logo" alt="homepage" /></span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class="nav-item hidden-sm-down search-box"> <a class="nav-link hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class=" mdi mdi-account-search"></i></a>
                            <form class="app-search">
                                <input type="text" class="form-control" placeholder="Search Name, Barcode"> <a class="srh-btn"><i class="mdi mdi-close"></i></a> </form>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{url('assets/images/users/1.jpg')}}" alt="user" class="profile-pic m-r-10" />{{Auth::user()->name}}</a>
                            <!-- item-->
                            <a href="{{ url('/logout') }}" class="link" data-toggle="tooltip"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                <i class="mdi mdi-power mdi-light"></i>
                            </a>
                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>

                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar" style="background-color: #f9f9f9;">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
      <!-- Sidebar navigation-->
      <div class="content">
            <nav>
                <div id="menu" class="menu">

                    <ul>
                        <li  ><a href="{{ url('/dashboard') }}"><i class="mdi mdi-monitor"></i>Dashboard</a></li>
                       
                        <!-- <li class="#"><a href="{{ url('/patient') }}"><i class="mdi mdi-account-plus"></i>
                        Test Enrollment</a></li> -->




                        <!-- <li><a onclick="$('#subDiagnosis').toggle();" href="#"><i class="mdi mdi-medical-bag"></i>Services</a>
                            <ul id="subDiagnosis" class="submenu"> -->
                               
                                <li><a href="{{ url('/laboratory') }}"><i class="mdi mdi-tumblr-reblog"></i>Laboratory</a></li>
                               

                              
                                <li><a href="{{ url('/Report') }}"><i class="mdi mdi-microscope"></i>Report</a></li>
                               

                               <!--  @role('microscopy_review')
                                <li><a href="{{ url('/microscopyreview') }}"><i class="mdi mdi-microscope"></i>Microscopy Review</a></li>
                                @endrole

                                @role('decontamination')
                                <li><a href="{{ url('/dash_decontamination') }}"><i class="mdi mdi-rhombus"></i>Decontamination</a></li>
                                @endrole

                                @role('decontamination_review')
                                <li><a href="{{ url('/decontamination') }}"><i class="mdi mdi-rhombus"></i>Decontamination Review</a></li>
                                @endrole

                                @role('dna_extraction')
                                <li><a href="{{ url('/DNAextraction') }}"><i class="mdi mdi-snowflake"></i>DNA extraction</a></li>
                                @endrole

                                @role('pcr')
                                <li><a href="{{ url('/PCR') }}"><i class="mdi mdi-snowman"></i>PCR</a></li>
                                @endrole

                                @role('hybridization')
                                <li><a href="{{ url('/hybridization') }}"><i class="mdi mdi-signal-hspa-plus"></i>Hybridization</a></li>
                                @endrole


                                @role('lpa_interpretation')
                                <li><a href="{{ url('/lpa_interpretation') }}"><i class="mdi mdi-stop"></i>LPA Interpretation</a></li>
                                @endrole

                                @role('culture_inoculation')
                                <li><a href="{{ url('/culture_inoculation') }}"><i class="mdi mdi-sigma-lower"></i>Culture inoculation</a></li>
                                @endrole

                                @role('lc_flagged_mgit')
                                <li><a href="{{ url('/lc_flagged_mgit') }}"><i class="mdi mdi-sign-caution"></i>LC Flagged MGIT Tube</a></li>
                                @endrole

                                @role('lc_flagged_mgit_further')
                                <li><a href="{{ url('/lc_flagged_mgit_further') }}"><i class="mdi mdi-sign-caution"></i>LC Flagged MGIT Tube Further</a></li>
                                @endrole

                                @role('lc_result_review')
                                <li><a href="{{ url('/lc_result_review') }}"><i class="mdi mdi-sign-caution"></i>Liquid Culture Result Review</a></li>
                                @endrole

                                @role('lj')
                                <li><a href="{{ url('/lj') }}"><i class="mdi mdi-shuffle-disabled"></i>LJ</a></li>
                                @endrole

                                @role('lj_review')
                                <li><a href="{{ url('/ljreview') }}"><i class="mdi mdi-shuffle-disabled"></i>LJ Review</a></li>
                                @endrole

                                @role('lc_dst_inoculation')
                                <li><a href="{{ url('/lc_dst_inoculation') }}"><i class="mdi mdi-source-commit-end"></i>LC - DST- Inoculation</a></li>
                                @endrole

                                @role('lj_dst_1st_line')
                                <li><a href="{{ url('/lj_dst_ln1') }}"><i class="mdi mdi-source-commit-end"></i>LJ - DST - 1st Line</a></li>
                                @endrole

                                @role('lj_dst_2st_line')
                                <li><a href="{{ url('/lj_dst_ln2') }}"><i class="mdi mdi-source-commit-end"></i>LJ - DST - 2nd Line</a></li>
                                @endrole

                                @role('microbiologist')
                                <li><a href="{{ url('/microbiologist') }}"><i class="mdi mdi-select-inverse"></i>Microbiologist</a></li>
                                @endrole

                                @role('barcodes')
                                <li><a href="{{ url('/barcodes') }}"><i class="mdi mdi-star"></i>Barcode Print</a></li>
                                @endrole

                                @role('hr')
                                <li><a href="{{ url('/hr') }}"><i class="mdi mdi-plus-box"></i>HR</a></li>
                                @endrole
                                
                                @role('equipment')
                                <li><a href="{{ url('/equipment') }}"><i class="mdi mdi-select"></i>Equipment</a></li>
                                @endrole

                                @role('change_role')
                                <li><a href="{{ url('/user_role') }}"><i class="mdi mdi-plus"></i>Add user role</a></li>
                                @endrole -->

                                <li><a href="{{ url('/adduser') }}"><i class="mdi mdi-plus"></i>Create New User</a></li>
                </div>
            </nav>
        </div>
      <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->

    <!-- Bottom points-->

    <!-- End Bottom points-->
  </aside>
  @yield('content')
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->

                <!-- <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Add Patient</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Add Patient</li>
                        </ol>
                    </div>
                </div> -->
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->


                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © Copyright Reserved 2017-2018, LIMS </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->


       <script src="{{ url('js/bootstrap-datepicker.min.js') }}"></script>





    <script type="text/javascript">
//     	$('#sandbox-container input').datepicker({
// });



        $(document).ready(function() {
            $('#example').DataTable();
        } );



    </script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ url('/assets/plugins/bootstrap/js/tether.min.js') }}"></script>

    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ url('js/jquery.slimscroll.js') }}"></script>
    <!--Wave Effects -->
    <script src="{{ url('js/waves.js') }}"></script>
    <!--Menu sidebar -->
    <script src="{{ url('js/sidebarmenu.js') }}"></script>
    <!--stickey kit -->
    <script src="{{ url('/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
    <!--Custom JavaScript -->
    <script src="{{ url('js/custom.min.js') }}">
    </script>
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- chartist chart
    <script src="{{ url('../assets/plugins/chartist-js/dist/chartist.min.js') }}"></script>
    <script src="{{ url('../assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js') }}"></script>
    <!--c3 JavaScript
    <script src="{{ url('../assets/plugins/d3/d3.min.js') }}"></script>
    <script src="{{ url('../assets/plugins/c3-master/c3.min.js') }}"></script>
    <!-- Chart JS -->
  <!--   <script src="{{ url('js/dashboard1.js') }}"></script> -->
    <script src="{{ url('js/common.js') }}"></script>

</body>

</html>
