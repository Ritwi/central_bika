<?php

namespace App\Model;
use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Data extends Model
{
    public static function movedata(){
      $data = DB::table('m_laboratory')->get();
      $tabledata = DB::table('t_services')->get();

      foreach ($data as $key => $value) {
        foreach ($tabledata as $key1 => $value1) {
          $data1 = json_decode(file_get_contents($value->url."/getdata/$value1->table_name/$value->lab_token"),true);
          foreach ($data1 as $key2 => $value2) {
            $value2['_id'] = $value2['id'];
            unset($value2['id']);
            DB::table($value1->table_name)->insert(
                $value2
            );
             $curl = curl_init();
             curl_setopt_array($curl, array(
               CURLOPT_RETURNTRANSFER => 1,
               CURLOPT_URL => $value->url."/updatedata",
               CURLOPT_POST => 1,
               CURLOPT_POSTFIELDS => array(
                 'id' => $value2['_id'],
                 'table_name' => $value1->table_name,
                 'lab_token' => $value->lab_token
               )
             ));
             $resp = curl_exec($curl);
             curl_close($curl);
             $result = json_decode($resp);
          }
        }
      }
      return true;
    }
}
