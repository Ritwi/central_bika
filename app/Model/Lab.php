<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Lab extends Model
{
    protected $table = 'm_laboratory';
    protected $fillable = ['name','location','in_charge','in_charge_number','in_charge_email','details','city'];
}
