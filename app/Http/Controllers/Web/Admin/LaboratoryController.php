<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;
use App\Model\Lab;
use App\Http\Controllers\Controller;

class LaboratoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['lab'] = Lab::orderBy('id','desc')->get();
        return view('admin.laboratory.list',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $data['lab'] = new Lab;
        return view('admin.laboratory.form',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
       
        $data = Lab::insert([
              'name' => $request->name,
              'code' => $request->code,
              'location' => $request->location,
              'in_charge' => $request->in_charge,
              'in_charge_number' => $request->in_charge_number,
              'in_charge_email' => $request->in_charge_email,
              'details' => $request->details,
              'city' => $request->city,
              'created_at' => date('Y-m-d H:i:s'),
              'updated_at' => date('Y-m-d H:i:s'),
            ]);
         return redirect('/laboratory');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lab = Lab::find($id);
            return view('admin.laboratory.form',['lab' => $lab]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $lab = Lab::find($id);
         //dd($request->all());
         $request->request->add(['created_at' => date('Y-m-d H:i:s')]);
         $request->request->add(['updated_at' => date('Y-m-d H:i:s')]);
         $lab->fill($request->all());
         $lab->save();
         return redirect('/laboratory');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
